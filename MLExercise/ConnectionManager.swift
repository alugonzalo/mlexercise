//
//  ConnectionManager.swift
//  MLExercise
//
//  Created by alu gonzalo on 21/11/15.
//  Copyright © 2015 alu gonzalo. All rights reserved.
//

import Foundation
import Alamofire

class ConnectionManager: AnyObject {
    
    class func getPaymentMethods(baseURL: String, uri: String, publicKey: String, response: (paymentMethods: [PaymentMethod], error: NSError?)->Void ) {
    
        let url = baseURL + uri
        
        let param = ["public_key" : publicKey ]
        
        Alamofire.request(.GET, url, parameters: param, encoding: ParameterEncoding.URL, headers: nil).responseJSON { (responseJSON : Response) -> Void in
            
            var arrPaymentMethod = [PaymentMethod]()
            
            print(responseJSON)
            
            if responseJSON.result.isSuccess {
                
                if let arrMethods = responseJSON.result.value as? [[String : AnyObject]] {
                    
                    for paymentMethodDict in arrMethods {
                        arrPaymentMethod.append(PaymentMethod(jsonDict: paymentMethodDict))
                    }
                }
                
                response(paymentMethods: arrPaymentMethod, error: nil)
                
            }else{
                response(paymentMethods: arrPaymentMethod, error: responseJSON.result.error)
            }
  
        }
        
    }
}