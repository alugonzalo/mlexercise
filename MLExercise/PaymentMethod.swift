//
//  PaymentMethod.swift
//  MLExercise
//
//  Created by alu gonzalo on 21/11/15.
//  Copyright © 2015 alu gonzalo. All rights reserved.
//

import Foundation

enum PaymentTypeID: String {
    case CreditCard = "credit_card"
    case Ticket = "ticket"
    case ATM = "atm"
    case Undefined = ""
}

class PaymentMethod {
    
    var id: String = ""
    var name: String = ""
    var paymentTypeID: PaymentTypeID = .Undefined
    var status: String = ""
    var secure_thumbnail: NSURL?
    var thumbnail: NSURL?
    var deferred_capture: String = ""
    var settings = Settings()
    var aditionalInfoNeeded = [AnyObject]()
    
    init(jsonDict: [String:AnyObject]){
    
        self.id = (jsonDict["id"] as? String) ?? ""
        self.name = (jsonDict["name"] as? String) ?? ""
        
        if let paymentTypeStr = jsonDict["payment_type_id"] as? String {
            self.paymentTypeID = PaymentTypeID(rawValue: paymentTypeStr) ?? .Undefined
        }

        self.status = (jsonDict["status"] as? String) ?? ""
        
        if let secure_thumbnailStr = jsonDict["secure_thumbnail"] as? String {
            self.secure_thumbnail = NSURL(string: secure_thumbnailStr)
        }
        
        if let thumbnail = jsonDict["thumbnail"] as? String {
            self.thumbnail = NSURL(string: thumbnail)
        }
        
        self.deferred_capture = (jsonDict["deferred_capture"] as? String) ?? ""
        
        if let settingsJson = jsonDict["settings"] as? [String: AnyObject] {
            self.settings = Settings(jsonDict: settingsJson)
        }
        
        self.aditionalInfoNeeded = (jsonDict["additional_info_needed"] as? [AnyObject]) ?? [AnyObject]()
    }
}


struct Settings {
    var bin = Bin()
    var cardNumber =  CardNumber()
    var securityCode = SecurityCode()
    
    init(){}
    
    init(jsonDict: [String:AnyObject]){
        
        if let bin = jsonDict["bin"] as? [String:AnyObject] {
            
            if let pattern = bin["pattern"] as? String {
                self.bin.pattern = pattern
            }
            
            if let exclusion = bin["exclusion_pattern"] as? String {
                self.bin.exclusionPattern = exclusion
            }
            
            if let installments = bin["installments_pattern"] as? String {
                self.bin.installmentsPattern = installments
            }
        }
        
        if let cardNumber = jsonDict["card_number"] as? [String:AnyObject] {
            
            if let length = cardNumber["length"] as? Int {
                self.cardNumber.lenght = length
            }
            
            if let validation = cardNumber["validation"] as? String {
                self.cardNumber.validation = validation
            }
        }
        
        if let securityCode = jsonDict["security_code"] as? [String:AnyObject] {
            
            if let mode = securityCode["mode"] as? String {
                self.securityCode.mode = mode
            }
            
            if let length = securityCode["length"] as? Int {
                self.securityCode.lenght = length
            }
            
            if let card_location = securityCode["card_location"] as? String {
                self.securityCode.cardLocation = card_location
            }
        }
        
    }
}

struct Bin {
    var pattern: String = ""
    var exclusionPattern: String = ""
    var installmentsPattern: String = ""
}

struct CardNumber {
    var lenght: Int = 0
    var validation: String = ""
}

struct SecurityCode {
    var mode: String = ""
    var lenght: Int = 0
    var cardLocation: String = ""
}







