//
//  PaymentMethodTableViewCell.swift
//  MLExercise
//
//  Created by alu gonzalo on 22/11/15.
//  Copyright © 2015 alu gonzalo. All rights reserved.
//

import UIKit

class PaymentMethodTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var paymentImage: UIImageView!
    
    static let cellIdentifier = "PaymentMethodTableViewCell"
    static let height: CGFloat = 100
    
    static var imagesCache = NSCache()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(paymentMethod: PaymentMethod){
        self.lblName.text = paymentMethod.name
        
        self.paymentImage.image = nil
        
        if let url = paymentMethod.thumbnail {
            setImageFromUrlInBackground(url)
        }
    }
    
    
    func setImageFromUrlInBackground(url: NSURL){
        
        //First, we verify if we have the image in the cache
        if let imageFromCache = PaymentMethodTableViewCell.imagesCache.objectForKey(url) as? UIImage {
            self.paymentImage.image = imageFromCache
            
        }else {
            
            let qualityOfServiceClass = QOS_CLASS_BACKGROUND
            let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
            dispatch_async(backgroundQueue, {
                
                if let dataFromUrl = NSData(contentsOfURL: url) {
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        let imageFromURL = UIImage.animatedImageWithAnimatedGIFData(dataFromUrl)
                        
                        //save image in cache
                        PaymentMethodTableViewCell.imagesCache.setObject(imageFromURL, forKey: url)
                        self.paymentImage.image = imageFromURL
                    })
                    
                }
            })
        }
    }
}
