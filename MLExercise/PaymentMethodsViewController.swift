//
//  ViewController.swift
//  MLExercise
//
//  Created by alu gonzalo on 19/11/15.
//  Copyright © 2015 alu gonzalo. All rights reserved.
//

import UIKit
import MBProgressHUD

class PaymentMethodsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var arrPaymentMethods = [PaymentMethod]()
    
    let baseURL = "https://api.mercadopago.com/v1/"
    let uri = "payment_methods"
    let publicKey = "444a9ef5-8a6b-429f-abdf-587639155d88"
    let paymentMethodIDToShow: PaymentTypeID = .CreditCard
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerNib(UINib(nibName: PaymentMethodTableViewCell.cellIdentifier, bundle: nil), forCellReuseIdentifier: PaymentMethodTableViewCell.cellIdentifier)
        
        
        getData()
    }
    
    func getData(){
        
        let progressHUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        ConnectionManager.getPaymentMethods(baseURL, uri: uri, publicKey: publicKey) { (paymentMethods, error) -> Void in
            
            progressHUD.hide(true)
            
            if error == nil {
                
                 self.arrPaymentMethods = paymentMethods.filter({ (paymentMethod) -> Bool in
                    if paymentMethod.paymentTypeID == self.paymentMethodIDToShow {
                        return true
                    }
                    return false
                })
                
                self.tableView.reloadData()
                
            }else {
                let alert = UIAlertController(title: "Error", message: error.debugDescription, preferredStyle: .Alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension PaymentMethodsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(PaymentMethodTableViewCell.cellIdentifier) as! PaymentMethodTableViewCell
        
        cell.configure(arrPaymentMethods[indexPath.row])
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return PaymentMethodTableViewCell.height
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPaymentMethods.count
    }
}

